# Cartographie Web des équipements de la CCLA


## Introduction

Ce site a été développé par l' [Agence Alpine des Territoires](https://agate-territoires.fr/). Il a été produit dans le cadre de l'accompagnement de l'agence sur l'analyse des équipements sportifs et culturels de la CCLA.

Le code est déposé sous la licence LGPL V 3.0 qui autorise la réutilisation du code sans dépôt obligatoire en OpenSource.


Le code utilise les bibliothèques suivantes
- Leaflet 1.7.1
- Bootstrap 5.0
- JQuery 3.6
- MarkerCluster 1.4
- Ajax Leaflet 1.0
- Handlebars 4.7.7

## Organisation du code


La page index.html contient l'ensemble de la structure de la page.  Avec :
- Une balise **header** pour le titre ;
- Une div **home** pour le bouton recentrant la carte ;
- Une div **content** pour la légende ;
- Une div **map** pour la carte.

Le style est défini par l'usage de classes Bootstrap et la CSS style.css. Ce dernier fichier contient également la définition de la class **.cluster** qui gère la représentation du regroupement de points.

Les images sont dans le dossier **img** avec un sous dossier spécifique pour les pictogrammes.

Les bibliothèques javascript et le script **script.js** se trouvent dans le dossier **js**. La fonction **init()** se déclenche au chargement de la page et charge la carte et l'action sur le bouton **home**.


Les données géographiques sont dans le dossier **geojson**. Il s'agit de fichiers au format GeoJSON - lisibles et éditables par un SIG - et au système de projection **EPSG:4326**.

3 jeux de données sont présents :
- Les contours des communes et des comcom de la CCLA qui proviennent de la Base IGN, Admin Express, 2021
- Les équipements  (fichier **equipement.geojson**) qui proviennent de la production d'Agate et de la BPE 2020 de l'INSEE.

# Mise à jour des équipements

## Ajout, suppression, modification d'équipements

Pour effectuer ces opérations, il suffit de modifier le  fichier **equipement.geojson** dans un SIG et de compléter les champs en respectant le format du tableau et en conservant le même système de projection.

Le point le plus important est de respecter les catégories pour qu'un picto puisse être associé à l'équipement. Vous retrouverez ci-dessous, les catégories et l'image associée :
- "Aire sportive de jeu/plateau EPS": "./pictos-CCLA-EPS.png",
- "Base d'aviron": "./pictos-CCLA-aviron.png",
- "Boulodrome": "./pictos-CCLA-boulodrome.png",
- "City stade": "./pictos-CCLA-citystade.png",
- "Gymnase": "./pictos-CCLA-gymnase.png",
- "Salle des fêtes/maison des asso.": "./pictos-CCLA-salle des fetes.png",
- "Site d'escalade": "./pictos-CCLA-escalade.png",
- "Tennis": "./pictos-CCLA-tennis.png",
- "Terrain d'entrainement": "./pictos-CCLA-terrain.png",
- "Terrain de foot": "./pictos-CCLA-football.png"
Il est indispensable de renseigner dans le fichier l'intitulé exacte, pra exemple **Terrain de foot**.

Si une autre catégorie venait à être entrée, l'équipement aurait un picto sans icone.

## Modification de la popup

Il est possible de modifier la popup pour changer l'organisation des informations ou d'ajouter d'autres informations.

Le contenu des popup est actuellement codé via un template de la bibliothèque Handlebars qui permet assez facilement de mettre à jour le contenu.

Il faut tout d'abord préparer dans le fichiers de données les informations que l'on souhaite ajouter à la popup (par exemple, une nouvelle colonne avec un nom __nomColonne__).

Il faut ensuite mettre à jour la variable **popUptmlTemplate** (ligne 179 du fichier script.js) pour prendre en compte ce nouveau nom de colonne deans le template. Cette variable est un bout de code HTML dans lequel on peut indiquer quelles informations de l'équipement sont utilisées.

Cette variable ressemble à :
```Javascript
'<h3> {{"Type d\'équipement"}}</h3>' +
'<span class="rose">Descriptif : </span> <span class="info">{{Descriptif}}</span> <br /> '
```
Le nom de la colonne à utiliser dans le fichier **equipement.js** est spécifié entre {{ }}.

Ainsi, on peut modifier cette variable pour intégrer les informations provenant de la colonne __nomColonne__.

```Javascript
'<h3> {{"Type d\'équipement"}}</h3>' +
'<span class="rose">Info de ma colonne : </span> <span class="info">{{__nomColonne__}}</span> <br /> '
'<span class="rose">Descriptif : </span> <span class="info">{{Descriptif}}</span> <br /> '
```
## Modification de la catégorisation

Il est possible d'ajouter de nouvelles catégories d'équipements pour faire cela, il faut suivre les étapes suivantes :
- Ajouter un picto dans le dossier **img/picto** au format PNG ou JPEG en respectant les dimensions des pictos déjà définis
- Ajouter dans le fichier de données d'équipement (equipement.geojson) les nouvelles données avec la nouvelle catégorie (**ATTENTION :** les mêmes intitulés doivent être utilisés pour désigner les même sclasses )
- Modifier la légende pour intégrer la nouvelle catégorie (div id='content') dans le fichier **index.html**
- Modifier le tableau dans **script.js** qui à chaque catégorie associe le picto qui lui est lié.

La variable
```Javascript
var assocCatPicto = {
  "Aire sportive de jeu/plateau EPS": "./pictos-CCLA-EPS.png",
```
deviendrait :

```Javascript
var assocCatPicto = {
  "Aire sportive de jeu/plateau EPS": "./pictos-CCLA-EPS.png",
  "Nouvelle catégorie": "./nouveau_picto.png",
```
